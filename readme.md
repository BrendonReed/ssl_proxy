# Setup up a https proxy for localhost services in docker

Based on https://medium.com/@oliver.zampieri/self-signed-ssl-reverse-proxy-with-docker-dbfc78c05b41 with insight from: https://dev.to/domysee/setting-up-a-reverse-proxy-with-nginx-and-docker-compose-29jg


1. Create a workspace ```mkdir docker_ssl_proxy```
1. Generate a key ```openssl req -subj '/CN=localhost' -extensions SAN -config <(cat /System/Library/OpenSSL/openssl.cnf <(printf "\n[SAN]\nsubjectAltName=DNS:localhost")) -x509 -newkey rsa:4096 -nodes -keyout key.pem -out cert.pem -days 365```
1. Import cert.pem into keychain and mark it as trusted
1. Create an nginx proxy file: 
    ```proxy_ssl.conf
    server {
    listen 443;
    ssl on;
    ssl_certificate /etc/nginx/conf.d/cert.pem;
    ssl_certificate_key /etc/nginx/conf.d/key.pem;
    location /plantuml {
        proxy_pass http://172.17.0.1:8090;
        rewrite ^/plantuml(.*)$ $1 break;
    }
    }
    ```

    To get the correct value for proxy_pass, run ```docker network inspect bridge``` and use the gateway value. The location path and proxy_pass port are based on the localhost services you wan to proxy to.
1. Run the nginx proxy docker container: ```docker run --name nginx_proxy -d -v `pwd`:/etc/nginx/conf.d -p 443:443 nginx```
1. Run the plantuml server: ```docker run -d -p 8090:8080 plantuml/plantuml-server:jetty```

Everything should work. Opening https://localhost/plantuml/png/0/SoWkIImgAStDuNBCoKnELT2rK_1DpCd9BwgqKWWepKlEu798pKi1oWC0 should render an image happily over https without any browser warnings.

To set up additional services, run them on other ports with docker or without, and add a location element to proxy_ssl.conf